function csvJSON(csv){

  var lines=csv.split("\n");

  var result = [];

  var headers=lines[0].split(",");

  for(var i=1;i<lines.length;i++){

	  var obj = {};
	  var currentline=lines[i].split(",");

	  for(var j=0;j<headers.length;j++){
		  obj[headers[j]] = currentline[j];
	  }

	  result.push(obj);

  }

  //return result; //JavaScript object
  return JSON.stringify(result); //JSON
}
async function apiToJson(name, func, seperator=",") {
	var response=await fetch("data/"+name+".csv");
	response=await response.text();
	func(JSON.parse(csvJSON(response, seperator)));
}
async function getStations(func) {
	var response=await fetch("https://api.deutschebahn.com/stada/v2/stations",{
        	headers: new Headers({
			'Authorization': 'Bearer a752daf98ebc6c66bb3af5f31853280b',
			'Content-Type': 'application/x-www-form-urlencoded',
			'Accept': 'application/json'
		}),
    	});
        response=await response.text();
        response=JSON.parse(response);
	var stations=[]
	response.result.forEach(function(station){
		try{
			stations[station.number]={
				name:station.name,
				coordinates:[
					station.evaNumbers[0].geographicCoordinates.coordinates[1],
					station.evaNumbers[0].geographicCoordinates.coordinates[0]
				]
			};
		}
		catch(error){
			//
		}
	})
	func(stations);
}

async function addStationsToMap(json) {
	json.forEach(function(station){
		L.marker(station.coordinates).addTo(mymap);
	})
}

async function drawEcoProblems() {
	fetch("data/eco_problems.json").then(function(response){return(response.json())}).then(function(json){
		Layers=[];
		var topicColors={
		    "terrestrial biological systems":"orange",
		    "cryosphere":"red",
		    "coastal processes and zones":"grey",
		    "hydrology":"blue",
		    "marine and freshwater biological systems":"yellow",
		    "agriculture and forestry":"green",
		};
		json.forEach(function(point){
			if(Layers[point.Category]==undefined) {
				Layers[point.Category]=L.layerGroup();
				Layers[point.Category].addTo(mymap);
			}
			L.marker([point.Lat, point.Long],{icon: Icons[topicColors[point.Category]]}).addTo(Layers[point.Category]).
				bindPopup("<b>"+point.Category+"</b><br />"+point.Full_ref+"<br/><i>Data from "+point.Yr_start+" to "+point.Yr_end+".</i><br />Published in by "+point.Ref);
		})
		layerList=document.getElementById("layerlist");
		Object.keys(Layers).forEach(function(layer){
			var li=document.createElement("li");
			li.classList.add(topicColors[layer]);
			li.innerHTML="<label><input type=\"checkbox\" checked ><span>"+layer+"</span></label>";
			li.children[0].children[0].addEventListener("click",function(){
				if(this.checked) {
					mymap.addLayer(Layers[layer]);
				} else {
					mymap.removeLayer(Layers[layer]);
				}
			});
			layerList.appendChild(li);
		})
	})
}
